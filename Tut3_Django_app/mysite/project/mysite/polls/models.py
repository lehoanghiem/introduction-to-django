from __future__ import unicode_literals
from django.utils.encoding import python_2_unicode_compatible

import datetime
from django.db import models
from django.utils import timezone


# For Python 3, use the first two imports above to use
# the decorator of Python 2, with the purpose of displaying
# the unicode texts that human can read from __str__ method
@python_2_unicode_compatible
# Create your models here.
class Question(models.Model):
    # has a question, is represented by an instance of a 'CharField'
    # class for character fields
    question_text = models.CharField(max_length=200)
    # and a publication date, which is the datetime,
    # described by 'DateTimeField'
    pub_date = models.DateTimeField('date published')

    # display the objects'representation, in interactive prompt
    # to easy to read
    def __str__(self):
        return self.question_text

    # adding a custom method, just for demonstration
    def was_published_recently(self):
        return self.pub_date >= timezone.now() - datetime.timedelta(days=1)


@python_2_unicode_compatible
class Choice(models.Model):
    # has two fields:
    # the text of choice, in char form with maximum length
    # of characters is 200
    choice_text = models.CharField(max_length=200)
    # and a vote total, with the default value to 0
    votes = models.IntegerField(default=0)
    # each 'Choice' is associated with a 'Question'
    question = models.ForeignKey(Question, on_delete=models.CASCADE)

    # display the objects'representation, in interactive prompt
    # to easy to read
    def __str__(self):
        return self.choice_text


'''
username = admin
email address: admin@example.com
password = thutha123
'''