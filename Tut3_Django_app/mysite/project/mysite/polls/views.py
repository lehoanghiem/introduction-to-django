from django.http import HttpResponse


# Create your views here.
def index(request):
    # create an HttpResponse object which is displayed when
    # accessing into the URL of polls
    return HttpResponse("Hello, world. You're at the polls index.")
