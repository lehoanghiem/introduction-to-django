# Mal the views.py to a URL

from django.conf.urls import url

from . import views

# create a url to be accessed
# this url pattern for view of polls
urlpatterns = [
    # argument: 'view' argument = calling the specified view function
    # after Django finding a regular expression matched.
    # argument: 'name' argument = naming the URL which is referred
    # clearly from elsewhere in Django
    url(r'^$', views.index, name='index'),
]